﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PagueVeloz
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class PagueVelozEntities : DbContext
    {
        public PagueVelozEntities()
            : base("name=PagueVelozEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<Empresa> Empresas { get; set; }
        public virtual DbSet<Fornecedor> Fornecedors { get; set; }
        public virtual DbSet<Fornecedor_Fisica> Fornecedor_Fisica { get; set; }
        public virtual DbSet<Fornecedor_Juridica> Fornecedor_Juridica { get; set; }
        public virtual DbSet<Telefone> Telefones { get; set; }
    }
}
