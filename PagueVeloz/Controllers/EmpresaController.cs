﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagueVeloz.Models;
using PagueVeloz.Util;

namespace PagueVeloz.Controllers
{
    public class EmpresaController : Controller
    {
        // GET: Empresa
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Lista()
        {
            PagueVelozEntities db = new PagueVelozEntities();
            return View(db.Empresas.ToList());
        }
        [HttpPost]
        public ActionResult Index(FormCollection form)
        {
            if (!CNPJ.isValid(form["emp_cnpj"]))
            {
                ModelState.AddModelError("emp_cnpj", "CNPJ inválido!");
                ViewData["Msg"] = "";


            }

            Empresa emp = new Empresa
            {
                emp_nome = form["emp_nome"],
                emp_cnpj = form["emp_cnpj"],
                emp_uf = form["emp_uf"]
            };

            if (ModelState.IsValid)
            {
                PagueVelozEntities db = new PagueVelozEntities();
                db.Empresas.Add(emp);
                db.SaveChanges();
                ViewData["Msg"] = "Cadastro efetuado com sucesso";
                emp = new Empresa();
            }
            return View(emp);
        }

    }
}