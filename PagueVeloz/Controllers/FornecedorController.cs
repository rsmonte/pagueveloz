﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagueVeloz.Util;
using PagueVeloz.Models;
using System.Data.Entity;

namespace PagueVeloz.Controllers
{
    public class FornecedorController : Controller
    {
        // GET: Fornecedor
        public ActionResult Index()
        {
            List<String> TEmpresa = new List<String>();

            PagueVelozEntities db = new PagueVelozEntities();
            ViewBag.SelectListEmpresas = new SelectList(db.Empresas.OrderBy(c => c.emp_nome).ToList(), "emp_id", "emp_nome");
            
            TEmpresa.Add("Pessoa Física");
            TEmpresa.Add("Pessoa Jurídica");

            ViewBag.TipoEmpresa = new SelectList(TEmpresa);
            return View();
        }
        [HttpPost]
        public ActionResult Index(FormCollection form)
        {
            PagueVelozEntities db = new PagueVelozEntities();
            ViewBag.SelectListEmpresas = new SelectList(db.Empresas.OrderBy(c => c.emp_nome).ToList(), "emp_id", "emp_nome");
            FornecedorPageModel model = new FornecedorPageModel();
            List<String> TEmpresa = new List<String>();

            TEmpresa.Add("Pessoa Física");
            TEmpresa.Add("Pessoa Jurídica");

            ViewBag.TipoEmpresa = new SelectList(TEmpresa);
            if (form["TipoEmpresa"] == "Pessoa Física")
            {
                Empresa emp = db.Empresas.Find(int.Parse(form["lbEmpresas"]));

                if(DateTime.Parse(form["fornecedorCPF.for_nascimento"]).AddYears(18) >= DateTime.Now && emp.emp_uf=="PR")
                    ModelState.AddModelError("fornecedorCPF.for_nascimento", "Pessoas físicas do PR devem ter mais de 18 anos");

                if (!CPF.isValid(form["fornecedorCPF.for_cpf"]))
                    ModelState.AddModelError("fornecedorCPF.for_cpf", "CPF Inválido");
            }
            else
            {
                if (!CNPJ.isValid(form["fornecedorCNJP.for_cnpf"]))
                    ModelState.AddModelError("ffornecedorCNJP.for_cnpf", "CNPJ Inválido");

            }







            Fornecedor_Fisica fornecedorFisica= new Fornecedor_Fisica();
            Fornecedor_Juridica fornecedor_Juridica= new Fornecedor_Juridica();
            Telefone tel = new Telefone();
                Fornecedor fornecedor = new Fornecedor
                {
                    for_nome = form["fornecedor.for_nome"],
                    emp_id = int.Parse(form["lbEmpresas"])

                };
                if (form["TipoEmpresa"] == "Pessoa Física")
                {
                    fornecedorFisica = new Fornecedor_Fisica
                    {
                        for_CPF = form["fornecedorCPF.for_cpf"],
                        for_RG = form["fornecedorCPF.for_RG"],
                        for_nascimento = DateTime.Parse(form["fornecedorCPF.for_nascimento"])
                    };


                }
                else
                {
                    fornecedor_Juridica = new Fornecedor_Juridica
                    {
                        for_cnpf = form["fornecedorCNJP.for_cnpf"]
                    };

                }

                string[] telefones = form["Telefones"].Split('\n');

                foreach (string str in telefones)
                {
                    tel = new Telefone
                    {
                        tel_numero = str
                    };

                }
            if (ModelState.IsValid)
            {
                db.Fornecedors.Add(fornecedor);
                db.SaveChanges();

                if (form["TipoEmpresa"] == "Pessoa Física")
                {
                    fornecedorFisica.for_id = fornecedor.for_id;
                    db.Fornecedor_Fisica.Add(fornecedorFisica);
                }
                else
                {   
                    fornecedor_Juridica.for_id = fornecedor.for_id;
                    db.Fornecedor_Juridica.Add(fornecedor_Juridica);
                }

                tel.for_id = fornecedor.for_id;
                db.Telefones.Add(tel);
                db.SaveChanges();

                ViewBag.Msg = "Cadastro Efetudo com sucesso";
                fornecedor = new Fornecedor();
                fornecedorFisica = new Fornecedor_Fisica();
                fornecedor_Juridica = new Fornecedor_Juridica();
                tel = new Telefone();

            }

            model.fornecedor = fornecedor;
            model.fornecedorCNJP = fornecedor_Juridica;
            model.fornecedorCPF = fornecedorFisica;
            model.telefone = tel;
            ViewBag.emp_id = int.Parse(form["lbEmpresas"]);


            return View(model);

        }

        public ActionResult Lista(string filtro, FormCollection form)
        {
            List<String> filtros = new List<String>();
            filtros.Add("Nome da Empresa");
            filtros.Add("Data Cadastro");

            ViewBag.filtros = new SelectList(filtros);

            PagueVelozEntities db = new PagueVelozEntities();

            List<FornecedorPageModel> modelo = new List<FornecedorPageModel>();

            System.Linq.IOrderedQueryable Fornecedores;
            switch (filtro)
            {
                case "Nome": 
                Fornecedores = db.Fornecedors.OrderBy(x => x.for_nome);
                    break;
                case "Data":
                    Fornecedores = db.Fornecedors.OrderBy(x => x.for_data_cad);
                    break;
                default:
                    Fornecedores = db.Fornecedors.OrderBy(x => x.for_nome);
                    break;
            }



            foreach (Fornecedor item in Fornecedores)
            {
                FornecedorPageModel iModelo = new FornecedorPageModel();
                iModelo.fornecedor = item;
                iModelo.empresa = db.Empresas.Where(x => x.emp_id == item.emp_id).FirstOrDefault();
                if(db.Fornecedor_Fisica.Where(x => x.for_id == item.for_id).FirstOrDefault() != null)
                    iModelo.fornecedorCPF = db.Fornecedor_Fisica.Where(x => x.for_id == item.for_id).FirstOrDefault();
                else
                    iModelo.fornecedorCNJP = db.Fornecedor_Juridica.Where(x => x.for_id == item.for_id).FirstOrDefault();
                modelo.Add(iModelo);


            }


            return View(modelo);
        }


    }
}