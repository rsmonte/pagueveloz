﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace PagueVeloz.Models
{
    public class FornecedorPageModel
    {
        public PagueVeloz.Empresa empresa { get; set; }
        public PagueVeloz.Telefone telefone { get; set; }
        public PagueVeloz.Fornecedor fornecedor { get; set; }
        public PagueVeloz.Fornecedor_Juridica fornecedorCNJP { get; set; }
        public PagueVeloz.Fornecedor_Fisica fornecedorCPF { get; set; }




    }
}