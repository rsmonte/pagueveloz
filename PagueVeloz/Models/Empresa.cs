﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
namespace PagueVeloz.Models
{
    public class Empresa
    {
        [Display(Name="Estado")]
        [Required(ErrorMessage = "Selecione o Estado!")]
        public string emp_uf { get; set; }
        [Display(Name="CNPJ")]
        [Required(ErrorMessage = "CNPJ Requerido!")]
        public string emp_cnpj { get; set; }
        [Required(ErrorMessage = "Nome Requirido!")]
        [Display(Name="Nome")]
        public string emp_nome { get; set; }
    }
    public enum Estados
    {
        AC,AL,AP,AM,BA,CE,DF,ES,GO,MA,MT,MS,MG,PA,PB,PR,PE,PI,RJ,RN,RS,RO,RR,SC,SP,SE,TO
    }
}