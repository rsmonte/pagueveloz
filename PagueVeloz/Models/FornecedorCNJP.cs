﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PagueVeloz.Models
{
    public class FornecedorCNJP
    {
        [Required(ErrorMessage = "CNPJ Requerido")]
        [Display(Name="CNPJ")]
        public string for_cnpj { get; set; }
    }
}