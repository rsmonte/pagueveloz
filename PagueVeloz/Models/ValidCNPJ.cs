﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using PagueVeloz.Util;


namespace PagueVeloz.Models
{
        public class ValidCNPJ : ValidationAttribute
        {
            protected override ValidationResult IsValid(object value,ValidationContext vc)
            {
            if (CNPJ.isValid(value.ToString()))
                return ValidationResult.Success;
            else
                return new ValidationResult("CNPJ Inválido");

        }
        }
}