﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PagueVeloz.Models
{
    public class Fornecedor
    {
        public int for_id { get; set; }
        
        public string for_nome { get; set; }

        public DateTime for_data_cad { get; set; }
        public int emp_id { get; set; }

    }
}