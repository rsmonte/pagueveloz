﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PagueVeloz.Models
{
    public class FornecedorCPF
    {
        [Required(ErrorMessage = "CPF Requerido")]
        [Display(Name="CPF")]
        public string for_cpf { get; set; }
        [Required(ErrorMessage = "RG Requerido")]
        [Display(Name="RG")]
        public string for_rg { get; set; }
        
        public DateTime for_data_nasc { get; set; }

    }
}