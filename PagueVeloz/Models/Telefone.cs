﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PagueVeloz.Models
{
    public class Telefone
    {
        [Required(ErrorMessage = "Telefone Requerido")]
        [Display(Name="Telefone")]
        public string tel_numero { get; set; }
        public int for_id { get; set; }
    }
}