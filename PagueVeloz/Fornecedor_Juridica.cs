//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PagueVeloz
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public partial class Fornecedor_Juridica
    {
        [Required(ErrorMessage = "CNPJ Requerido")]
        [Display(Name = "CNPJ")] 
        public string for_cnpf { get; set; }
        public int for_id { get; set; }
        public int for_pj_id { get; set; }
    
        public virtual Fornecedor Fornecedor { get; set; }
    }
}
